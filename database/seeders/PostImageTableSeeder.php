<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\Category;
use App\Models\PostImage;
use Illuminate\Database\Seeder;

class PostImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PostImage::truncate();//metodo para borrar la tabla
        //metodo para crear un registro
        
        $posts = Post::all();

        foreach($posts as $key => $p){
            PostImage::create([
                'image' => '1623297609.png',
                'post_id' => $p->id
            ]);            
        }
    }

}
