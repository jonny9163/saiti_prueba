<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreatePostTest extends TestCase
{
    /** 
        * @test
     */
    public function test_un_usuario_puede_crear_post()
    {
        //1. Given => teniendo un usuario autenticado
        // $user = factory(User::class)->create();
        $user = User::factory()->create();
        $this->actingAs($user);
        //2. When => cuando hace un post request a store post controller 
        $this->post(route('post.store'), ['title'=>'Mi primer post'] );
        //3. Then => entonces veo un uevo post en base de datos
        $this->assertDatabaseHas('posts',[
           'title' => 'Mi primer post'
        ]);
        // $this->assertTrue(true);
    }
}
